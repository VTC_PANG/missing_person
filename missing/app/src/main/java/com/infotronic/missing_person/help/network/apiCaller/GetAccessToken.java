package com.infotronic.missing_person.help.network.apiCaller;


import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class GetAccessToken {

    public static void getAccessToken(){
        Map map = new HashMap();
        String email = Config.getUname();
        String pwd = Config.getPwd();
        map.put("email", email);
        map.put("password", pwd);
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.post(Config.BASE_URL + Config.API_USER + Config.API_TOKEN)
                .addHeaders(NetworkHeaderHelper.getDefaultHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        Log.d("debug_paul", "token response:" + response.getResult().getToken());
                        Log.d("debug_paul", "getStatus:" + response.getStatus());
                        if("200".equals(response.getStatus())){
                            Config.setTokenAccess(response.getResult().getToken());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError asdfasdf:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }



}
