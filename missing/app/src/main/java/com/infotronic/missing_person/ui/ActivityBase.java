package com.infotronic.missing_person.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.error.ANError;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.help.appbase.CustomParsedRequestListener;
import com.infotronic.missing_person.service.db.impl.FoodlibsServiceImpl;
import com.infotronic.missing_person.service.db.impl.MPersonServiceImpl;
import com.infotronic.missing_person.service.db.impl.UsersServiceImpl;

import timber.log.Timber;

public abstract class ActivityBase extends AppCompatActivity {
    //===============db caller=======================================
    public static UsersServiceImpl userService = null;
    public static FoodlibsServiceImpl foodlibsService = null;
    public static MPersonServiceImpl mPersonService = null;
    //    public static CompaniesServiceImpl companiesService = null;
    //===============================================================
    private Dialog dialog;
    final public CustomParsedRequestListener.CallbackDialog dismissDialogCallback =
            new CustomParsedRequestListener.CallbackDialog() {
                @Override
                public void dismissDialog() {
                    ActivityBase.this.dismissDialog();
                }

                @Override
                public void errorDialog(ANError anError) {
                    ActivityBase.this.showInteractDialog(anError.getErrorBody());
                    Timber.d(anError.getCause());
                }
            };


    public abstract Context getActivityContext();

    public abstract int getContentViewRid();

    public void initDbService(Context context) {
        if (userService == null)
            userService = new UsersServiceImpl(context);
        if (foodlibsService == null)
            foodlibsService = new FoodlibsServiceImpl(context);
        if (mPersonService == null)
            mPersonService = new MPersonServiceImpl(context);


    }

    public void resetDbService() {
        userService = null;
        foodlibsService = null;
        mPersonService = null;
        initDbService(getActivityContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewRid());
        initDbService(getActivityContext());
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        dismissDialog();
    }

    final public void showDialog(String title, String msg) {
        dialog = ProgressDialog.show(getActivityContext(),
                title, msg, true);
    }

    final public void showDialog(String msg) {
        dialog = ProgressDialog.show(getActivityContext(),
                null, msg, true);
    }

    final public void showLoadingDialog() {
        dismissDialog();
        dialog = ProgressDialog.show(getActivityContext(),
                null, "載入中...", true);
    }

    final public void showInteractDialog(String msg) {
        String error = "";
        error = error + msg;
        if (error.contains("invalid_token")) {
            new MaterialAlertDialogBuilder(getActivityContext()).setMessage(error)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();
        } else {
            new MaterialAlertDialogBuilder(getActivityContext()).setMessage(msg)
                    .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    })
                    .show();
        }
    }

    final public void dismissDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    final boolean isDialogShowing() {
        if (dialog != null) {
            return dialog.isShowing();
        }
        return false;
    }

    final public void setupOnclickItems(int[] onclickList, View.OnClickListener onClickListener) {
        for (int rid : onclickList) {
            findViewById(rid).setClickable(true);
            findViewById(rid).setFocusable(true);
            findViewById(rid).setOnClickListener(onClickListener);
        }
    }

}
