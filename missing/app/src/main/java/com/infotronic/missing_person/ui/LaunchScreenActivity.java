package com.infotronic.missing_person.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;
import androidx.transition.TransitionInflater;
import androidx.transition.TransitionSet;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.db.AppDatabase;
import com.infotronic.missing_person.help.appbase.Base64Util;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.apiCaller.GetAccessToken;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.fragment.login.LaunchScreenFragment;
import com.infotronic.missing_person.ui.fragment.login.LoginFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Response;
import timber.log.Timber;


public class LaunchScreenActivity extends ActivityBase {

    private static final long MOVE_DEFAULT_TIME = 1000;
    private static final long FADE_DEFAULT_TIME = 300;


    private FragmentManager mFragmentManager;

    private String[] permissionList = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };

    private Timer timer = new Timer();

    @Override
    public Context getActivityContext() {
        return LaunchScreenActivity.this;
    }

    @Override
    public int getContentViewRid() {
        return R.layout.activity_lanuch_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        mFragmentManager = getSupportFragmentManager();
        Log.d("debug_paul", "onCreate");

        loadInitialFragment();
        grantPermission();

    }


    final Thread setupThread = new Thread() {
        @Override
        public void run() {
            try {
                super.run();
                if (onLoad()) {
                    Log.d("debug_paul", "onLoad");
                    performTransition();
                } else {
                    throw new Exception("Missing File");
                }
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage());
            }
        }
    };


    private void loadInitialFragment() {
        Fragment initialFragment = LaunchScreenFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, initialFragment);
        fragmentTransaction.commit();
    }

    private void onback() {
        Fragment initialFragment = LoginFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, initialFragment);
        fragmentTransaction.commit();
    }

    private void performTransition() {
        // more on this later
        if (isDestroyed()) {
            return;
        }
        Fragment previousFragment = mFragmentManager.findFragmentById(R.id.login_fragment_container);
        Fragment nextFragment = new LoginFragment();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        // 1. Exit for Previous Fragment
        Fade exitFade = new Fade();
        exitFade.setDuration(FADE_DEFAULT_TIME);
        previousFragment.setExitTransition(exitFade);

        // 2. Shared Elements Transition
        TransitionSet enterTransitionSet = new TransitionSet();
        enterTransitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move));
        enterTransitionSet.setDuration(MOVE_DEFAULT_TIME);
        enterTransitionSet.setStartDelay(FADE_DEFAULT_TIME);
        nextFragment.setSharedElementEnterTransition(enterTransitionSet);

        // 3. Enter Transition for New Fragment
        Fade enterFade = new Fade();
        enterFade.setStartDelay(MOVE_DEFAULT_TIME + FADE_DEFAULT_TIME);
        enterFade.setDuration(FADE_DEFAULT_TIME);
        nextFragment.setEnterTransition(enterFade);

        View logo = findViewById(R.id.logo);

        fragmentTransaction.addSharedElement(logo, logo.getTransitionName());
        fragmentTransaction.replace(R.id.login_fragment_container, nextFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected boolean onLoad() {
        SystemClock.sleep(800);
        return true;
    }


    private void grantPermission() {
        List<String> permissionListToBeGrant = new ArrayList<String>();
        for (String permission : permissionList) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionListToBeGrant.add(permission);
            }
        }

        if (permissionListToBeGrant.size() > 0) {
            String[] permissionListToBeGrantArray = new String[permissionListToBeGrant.size()];
            permissionListToBeGrantArray = permissionListToBeGrant.toArray(permissionListToBeGrantArray);
            Log.d(getLocalClassName(), "permissionListToBeGrantArray: " + permissionListToBeGrantArray.length);
            ActivityCompat.requestPermissions(this, permissionListToBeGrantArray, 1);
        } else {
            SharedPreferences settings = getSharedPreferences(Config.PREF_KEY, MODE_PRIVATE);
            String encode = settings.getString(Config.PREF_Login_KEY, "");
            if (!encode.equals("")) {
                Log.d(getLocalClassName(), "Auto Login");
                Log.d(getLocalClassName(), Base64Util.getUname(encode) + Base64Util.getPwd(encode));
                Config.setUname(Base64Util.getUname(encode));
                Config.setPwd(Base64Util.getPwd(encode));
                GetAccessToken.getAccessToken();
                goToMain();
                return;
            }
            setupThread.start();
//            getAccessToken("Tom", "leo123", false);
        }
    }

    private void fail() {
        new MaterialAlertDialogBuilder(LaunchScreenActivity.this)
                .setMessage(getResources().getString(R.string.error_username_password))
                .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                })
                .show();
    }

    public void getAllData() {
        dismissDialog();
        showDialog("Downing Data...");
        Timber.d("Get Data");

        AndroidNetworking.get(Config.BASE_URL + Config.API_SYNC + Config.API_GET_MP)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        Log.d("debug_paul", "response:" + response.getResult().getmPerson().toString());
                        mPersonService.insertAll(response.getResult().getmPerson());


                        Log.d("debug_paul", "mPersonService:" + mPersonService.getById("608fcf05b49ee9c726b2aef4"));

                        dismissDialog();

//                        userRolesService.insertAll(response.getResult().getUserRoles());
                        goToMain();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    public void getUserInfo() {
        dismissDialog();
        showDialog("Getting user data...");
        Timber.d("Getting user data");

        AndroidNetworking.get(Config.BASE_URL + Config.API_SYNC + Config.API_USER_INFO)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        AppDatabase.createDB(getActivityContext());
                        resetDbService();
                        userService.insertAll(response.getResult().getUser());
                        Config.setUid(response.getResult().getUser().get_id());
                        Log.d("debug_paul", "123123 Config.getUid(): " + Config.getUid());
                        Config.setUname(response.getResult().getUser().getName());
                        Log.d("debug_paul", "user info from db:" + userService.getById(Config.getUid()));
                        SharedPreferences settings = getSharedPreferences(Config.PREF_KEY, MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(Config.PREF_USER_ID_KEY, response.getResult().getUser().get_id());
                        editor.apply();
                        getAllData();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    public void getAccessToken(final String email, final String pwd,
                               final boolean ischecked) {
        showDialog("Login...");
        Map map = new HashMap();
        map.put("email", email);
        map.put("password", pwd);
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.post(Config.BASE_URL + Config.API_USER + Config.API_TOKEN)
                .addHeaders(NetworkHeaderHelper.getDefaultHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        Log.d("debug_paul", "token response:" + response.getResult().getUser().toString());
                        if ("200".equals(response.getStatus())) {
                            Config.setTokenAccess(response.getResult().getToken());
                            Config.setUname(email);
                            Config.setPwd(pwd);
                            SharedPreferences settings = getSharedPreferences(Config.PREF_KEY, MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            Log.d("debug_paul", response.getResult().getToken());
                            if (ischecked) {
                                editor.putString(Config.PREF_Login_KEY, Base64Util.getEncodeStr(email, pwd));
                            }
                            editor.apply();

//                            goToMain();
                            getAllData();
//                            getUserInfo();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError: :" + anError);

                        dismissDialog();
                        fail();
                    }
                });
    }

    public void goToMain() {
        dismissDialog();
        Intent intent = new Intent(LaunchScreenActivity.this, MainActivity.class);
        startActivity(intent);
    }


    public void login(String username, final String password, boolean ischecked) {
        Log.d("LoginActivity", "login");
        getAccessToken(username, password, ischecked);
    }

    public void register(String uname, String pwd, String email) {
        Map map = new HashMap();
        map.put("name", uname);
        map.put("password", pwd);
        map.put("email", email);
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.post(Config.BASE_URL + Config.API_UPDATE + Config.API_REGISTER)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            showDialog("Sign up successful\nPlease login again");
                            timer.cancel();
                            timer = new Timer();
                            timer.schedule(
                                    new TimerTask() {
                                        @Override
                                        public void run() {
                                            dismissDialog();
                                            onback();
                                        }
                                    },1000
                            );

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        showDialog("Sign up fail\nPlease check the error");
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        dismissDialog();
                                    }
                                },1500
                        );
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    //hide soft keyboard when tap outside the editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            View view = this.getCurrentFocus();
            view.clearFocus();
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                grantPermission();
                break;
        }
    }


}
