package com.infotronic.missing_person.help.appbase;

public interface FragmentBackHandler {
    boolean onBackPressed();
}
