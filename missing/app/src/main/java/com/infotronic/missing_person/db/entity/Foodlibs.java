package com.infotronic.missing_person.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Foodlibs {
    @PrimaryKey
    @NonNull
    private int foodlib_id;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "subtype")
    private String subtype;

    @ColumnInfo(name = "food_name")
    private String food_name;

    @ColumnInfo(name = "calories")
    private int calories;

    @ColumnInfo(name = "portion")
    private int portion;


    public Foodlibs(int foodlib_id, String type, String subtype, String food_name, int calories, int portion) {
        this.foodlib_id = foodlib_id;
        this.type = type;
        this.subtype = subtype;
        this.food_name = food_name;
        this.calories = calories;
        this.portion = portion;
    }


    @Override
    public String toString() {
        return "Foodlib{" +
                "foodlib_id=" + foodlib_id +
                ", type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                ", food_name='" + food_name + '\'' +
                ", calories=" + calories +
                ", portion=" + portion +
                '}';
    }

    public int getFoodlib_id() {
        return foodlib_id;
    }

    public void setFoodlib_id(int foodlib_id) {
        this.foodlib_id = foodlib_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getFood_name() {
        return food_name;
    }

    public void setFood_name(String food_name) {
        this.food_name = food_name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getPortion() {
        return portion;
    }

    public void setPortion(int portion) {
        this.portion = portion;
    }
}
