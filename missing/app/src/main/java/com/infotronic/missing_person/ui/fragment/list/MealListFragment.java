package com.infotronic.missing_person.ui.fragment.list;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.help.appbase.FragmentBackHandler;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.adapter.MealListAdapter;
import com.infotronic.missing_person.ui.fragment.HomeViewModel;
import com.infotronic.missing_person.ui.viewModel.MealListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MealListFragment extends FragmentBase implements FragmentBackHandler {


    @BindView(R.id.meal_toolbar_list)
    MaterialToolbar mealToolbarList;
    @BindView(R.id.tv_trade_item_title)
    TextView tvTradeItemTitle;
    @BindView(R.id.tv_trade_item_title_score)
    TextView tvTradeItemTitleScore;
    @BindView(R.id.test_recyclerView)
    RecyclerView testRecyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.appbar)
    CoordinatorLayout appbar;
    private String TAG = "MealListFragment";
    private HomeViewModel homeViewModel;
    private static final String ARG_TYPE = "ARG_TYPE";
    private static final String ARG_COLOR = "ARG_COLOR";
    private String type;
    private String color;

    private MealListAdapter mealListAdapter;

    //butter knife
    private Unbinder unbinder;

    //fragment
    private FragmentManager mFragmentManager;

    //db control
    ArrayAdapter<String> adapter;

    public static MealListFragment newInstance() {
        return new MealListFragment();
    }

    public static MealListFragment newInstance(String type) {
        MealListFragment mealListFragment = new MealListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        mealListFragment.setArguments(args);
        return mealListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_meal_list, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        testRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        onRefreshView();

        return root;
    }


    private void onRefreshView() {

        mealListAdapter = new MealListAdapter(getContext());
        mealToolbarList.setNavigationOnClickListener(navOnClickListener);
        testRecyclerView.setAdapter(mealListAdapter);
        initDataList();
        mealListAdapter.setOnItemClickListner(onItemClickListner);
    }

    private MealListAdapter.onItemClickListner onItemClickListner = new MealListAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_paul", "description: " + description);
        }
    };

    private void initDataList() {
        List<String> description = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            Log.d("debug_paul", "i: " + i);
            Log.d("debug_paul", "description.size(): " + description.size());
            description.add("Test " + i);
        }
        List<MealListViewModel> mealListViewModels = new ArrayList<>();
        for (int i = 0; i < description.size(); i++) {
            mealListViewModels.add(new MealListViewModel(description.get(i)));
        }
        mealListAdapter.addModels(mealListViewModels);
    }


    @Override
    public int getFragmentId() {
        return R.layout.fragment_meal_list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*custom a Fragment back button */
    @Override
    public boolean onBackPressed() {
        onBack();
        return false;

    }

    public void onBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DAILY_FORM_FRAGMENT);
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.MEAL_LIST_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBack();
        }
    };

    public void transactionFragment(Fragment fragment) {
//        mFragmentManager.beginTransaction().replace(R.id.checklist, fragment, Config.LIST_FRAGMENT).addToBackStack(Config.LIST_FRAGMENT).commit();
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.MEAL_LIST_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.ADD_MEAL_FRAGMENT);
        fragmentTransaction.show(fragment);
//        fragmentTransaction.hide(curFragment);
        fragmentTransaction.addToBackStack(Config.MEAL_LIST_FRAGMENT);
        fragmentTransaction.commit();
    }


    @OnClick(R.id.fab)
    public void onViewClicked() {
        Fragment selectedFragment = AddMealFragment.newInstance();
        transactionFragment(selectedFragment);
    }
}
