package com.infotronic.missing_person.ui.viewModel;

import androidx.lifecycle.ViewModel;

import com.infotronic.missing_person.db.entity.MPerson;

public class MPersonViewModel extends ViewModel {
    public MPerson mPerson;

    public MPersonViewModel() {}

    public MPersonViewModel(MPerson mPerson) {
        this.mPerson = mPerson;
    }

}