package com.infotronic.missing_person.ui.fragment.setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.infotronic.missing_person.R;
import com.infotronic.missing_person.help.appbase.FragmentBackHandler;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.MainActivity;
import com.infotronic.missing_person.ui.fragment.HomeViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingFragment extends FragmentBase implements FragmentBackHandler {

    @BindView(R.id.btn_delete)
    Button btnDelete;
    private String TAG = "MealListFragment";
    private HomeViewModel homeViewModel;

    //butter knife
    private Unbinder unbinder;

    //fragment
    private FragmentManager mFragmentManager;

    //db control
    ArrayAdapter<String> adapter;

    public static SettingFragment newInstance() {
        return new SettingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_setting;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public boolean onBackPressed() {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.backToMain();
        }
        return false;

    }

    @OnClick(R.id.btn_delete)
    public void onViewClicked() {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.logout();
        }
    }
}
