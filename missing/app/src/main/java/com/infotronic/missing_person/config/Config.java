package com.infotronic.missing_person.config;

public class Config {
    public static String BASE_URL = "http://192.168.1.28:10900";
//    public static String BASE_URL = "http://221.124.83.191:8443";

    //api
    final public static String API_TOKEN = "/login";
    final public static String API_USER = "/users";
    final public static String API_SYNC = "/sync";
    final public static String API_UPDATE = "/update";
    final public static String API_USER_INFO = "/userinfo";
    final public static String API_CHECK_USER_NAME = "/checkUsername";
    final public static String API_REGISTER = "/register";
    final public static String API_ADD_PERSON = "/newPerson";
    final public static String API_GET_PERSON_BY_NAME = "/getPersonByName";
    final public static String API_DELETE_PERSON_BY_NAME = "/deletePerson";
    final public static String API_UPDATE_PERSON_BY_NAME = "/updatePerson";
    final public static String API_FOODLIB = "/foodlib";
    final public static String API_GET_MP = "/allPerson";

    //shared prefs
    final public static String PREF_KEY = "checklist";
    final public static String VERSION_KEY = "checklist_version";
    final public static String PREF_Login_KEY = "login-preference";
    final public static String PREF_USER_ID_KEY = "userId-preference";
    final public static String PREF_LANG = "lang-preference";
    final public static String PREF_VERSION_CODE = "version-preference";
    final public static String PREF_DB_VERSION_CODE = "dbVersion-preference";
    final public static String PREF_DB_USER = "dbUser-preference";
    final public static String PREF_SYNC_TIME = "syncTime-preference";

    //Fragment
    final public static String LIST_FRAGMENT = "LIST_FRAGMENT";
    final public static String LIST_ITEM_FRAGMENT = "LIST_ITEM_FRAGMENT";
    final public static String DAILY_FORM_FRAGMENT = "DAILY_FORM_FRAGMENT";
    final public static String MEAL_LIST_FRAGMENT = "MEAL_LIST_FRAGMENT";
    final public static String HOME_FRAGMENT = "HOME_FRAGMENT";
    final public static String ADD_MEAL_FRAGMENT = "ADD_MEAL_FRAGMENT";
    final public static String PERSON_INFO_FRAGMENT = "PERSON_INFO_FRAGMENT";
    final public static String HISTORY_FRAGMENT = "HISTORY_FRAGMENT";
    final public static String HISTORY_PROJECT_FRAGMENT = "HISTORY_PROJECT_FRAGMENT";
    final public static String HISTORY_ITEM_FRAGMENT = "HISTORY_ITEM_FRAGMENT";
    final public static String SETTING_FRAGMENT = "SETTING_FRAGMENT";
    final public static String REPORT_FRAGMENT = "REPORT_FRAGMENT";
    final public static String REPORT_TYPE_FRAGMENT = "REPORT_TYPE_FRAGMENT";
    final public static String REPORT_PROJECT_FRAGMENT = "REPORT_PROJECT_FRAGMENT";
    final public static String REPORT_PROJECT_UNIT_FRAGMENT = "REPORT_PROJECT_UNIT_FRAGMENT";
    final public static String ADD_ITEM_FRAGMENT = "ADD_ITEM_FRAGMENT";
    final public static String CHANGE_PWD_FRAGMENT = "CHANGE_PWD_FRAGMENT";
    final public static String DUMMY_FRAGMENT = "DUMMY_FRAGMENT";

    //create folder path
    public static String FOLDER_PATH = "Checklist";

    //user data
    private static String uname = "";
    private static String uid = "-1";
    private static String pwd = "";
    private static String TOKEN_ACCESS = "";
    public static boolean clickable = false;

    public static String getUname() {
        return uname;
    }

    public static void setUname(String uname) {
        Config.uname = uname;
    }

    public static String getUid() {
        return uid;
    }

    public static void setUid(String uid) {
        Config.uid = uid;
    }

    public static String getPwd() {
        return pwd;
    }

    public static void setPwd(String pwd) {
        Config.pwd = pwd;
    }

    public static boolean isClickable() { return clickable; }

    public static void setClickable(boolean clickable) { Config.clickable = clickable; }

    public static String getTokenAccess() {
        return TOKEN_ACCESS;
    }

    public static void setTokenAccess(String tokenAccess) {
        TOKEN_ACCESS = "Bearer " + tokenAccess;
    }

    public static void flushData(){
        uid = "-1";
        uname = "";
    }

}
