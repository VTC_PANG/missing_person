package com.infotronic.missing_person.service;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;

import java.util.List;

public interface MPersonService {
    MPerson getById(String _id);

    List<MPerson> getByGender(String gender);

    void insertAll(List<MPerson> mPersonList);

    void insert(MPerson mPerson);

    void update(MPerson mPerson);

    void deleteById(String id);

    List<MPerson> getAll();

    LiveData<List<MPerson>> getAllMPerson();
}
