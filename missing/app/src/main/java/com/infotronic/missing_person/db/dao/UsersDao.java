package com.infotronic.missing_person.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.missing_person.db.entity.User;

@Dao
public interface UsersDao {
    @Query("SELECT * FROM User WHERE _id = :id")
    User getById(String id);

    @Query("SELECT * FROM User WHERE name = :name")
    User getByUsername(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User user);

    @Update
    void update(User user);
}
