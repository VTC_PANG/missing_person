package com.infotronic.missing_person.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.infotronic.missing_person.db.dao.FoodlibsDao;
import com.infotronic.missing_person.db.dao.MPersonDao;
import com.infotronic.missing_person.db.dao.UsersDao;
import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.db.entity.User;

import org.jetbrains.annotations.NotNull;

import java.io.File;

@Database(
    entities = {
        User.class,
        Foodlibs.class,
        MPerson.class
    },
    version = 1,
    exportSchema = false
)

public abstract class AppDatabase extends RoomDatabase {
    //LocalDB config
    private static String DB_NAME = "foodcalDB";
    private static AppDatabase appDatabase = null;

    public static AppDatabase getInstance(Context context) {
        String USER_DB_NAME;
//        SharedPreferences version = context.getSharedPreferences(Config.PREF_KEY, Context.MODE_PRIVATE);
//        SharedPreferences settings = context.getSharedPreferences(Config.VERSION_KEY, Context.MODE_PRIVATE);
//
//        int uid = version.getInt(Config.PREF_USER_ID_KEY, -1);
//
//        String encode = version.getString(Config.PREF_Login_KEY, "");
//        if (encode.equals("")) {
//            if (Config.getUid() != -1) {
//                USER_DB_NAME = Config.getUid() + "_" + DB_NAME;
//            } else {
//                USER_DB_NAME = DB_NAME;
//            }
//        } else {
//            if (uid != -1) {
//                USER_DB_NAME = uid + "_" + DB_NAME;
//            } else {
//                if (Config.getUid() != -1) {
//                    USER_DB_NAME = Config.getUid() + "_" + DB_NAME;
//                } else {
//                    USER_DB_NAME = DB_NAME;
//                }
//            }
//        }
        USER_DB_NAME = DB_NAME;
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, USER_DB_NAME)
                    .build();
        }
        return appDatabase;
    }

    public static void deleteDB(@NotNull Context context) {
        File[] allDb = new File(context.getApplicationInfo().dataDir + "/databases").listFiles();
        if (allDb != null) {
            for (final File file : allDb) {
                file.delete();
            }
        }
    }

    public static void createDB(Context context) {
        appDatabase = null;
        getInstance(context);
    }

    public abstract UsersDao usersDao();
    public abstract FoodlibsDao foodlibsDao();
    public abstract MPersonDao mPersonDao();

}
