package com.infotronic.missing_person.service.db.impl;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;


import com.infotronic.missing_person.db.AppDatabase;
import com.infotronic.missing_person.db.dao.FoodlibsDao;
import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.service.FoodlibsService;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class FoodlibsServiceImpl implements FoodlibsService {
    private FoodlibsDao foodlibsDao;
    private LiveData<List<Foodlibs>> allFoodlibs;

    public FoodlibsServiceImpl(Context context){
        foodlibsDao = AppDatabase.getInstance(context).foodlibsDao();
        allFoodlibs = foodlibsDao.getAllFoodblis();
    }

    @Override
    public Foodlibs getById(final int id) {
        try {
            Foodlibs projects = new AsyncTask<Void, Void, Foodlibs>() {
                @Override
                protected Foodlibs doInBackground(Void... voids) {
                    return foodlibsDao.getById(id);
                }
            }.execute().get();
            return projects;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Foodlibs> getByType(String type) {
        try {
            List<Foodlibs> foodlibsList = new AsyncTask<Void, Void, List<Foodlibs>>() {
                @Override
                protected List<Foodlibs> doInBackground(Void... voids) {
                    return foodlibsDao.getByType(type);
                }
            }.execute().get();
            return foodlibsList;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Foodlibs> getBySubType(String subtype) {
        try {
            List<Foodlibs> foodlibsList = new AsyncTask<Void, Void, List<Foodlibs>>() {
                @Override
                protected List<Foodlibs> doInBackground(Void... voids) {
                    return foodlibsDao.getBySubType(subtype);
                }
            }.execute().get();
            return foodlibsList;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public LiveData<List<Foodlibs>> getAllFoodblis() {
        return allFoodlibs;
    }

    @Override
    public void insertAll(final List<Foodlibs> foodlibs) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                foodlibsDao.insertAll(foodlibs);
                return null;
            }
        }.execute();
    }

    @Override
    public void insert(Foodlibs foodlibs) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                foodlibsDao.insert(foodlibs);
                return null;
            }
        }.execute();
    }

    @Override
    public void update(Foodlibs foodlibs) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                foodlibsDao.update(foodlibs);
                return null;
            }
        }.execute();
    }

}
