package com.infotronic.missing_person.ui.fragment.list;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.help.appbase.FragmentBackHandler;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.MainActivity;
import com.infotronic.missing_person.ui.adapter.MPersonListAdapter;
import com.infotronic.missing_person.ui.adapter.MealListAdapter;
import com.infotronic.missing_person.ui.fragment.HomeViewModel;
import com.infotronic.missing_person.ui.viewModel.MPersonViewModel;
import com.infotronic.missing_person.ui.viewModel.MealListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.Response;

public class DailyFormFragment extends FragmentBase implements FragmentBackHandler {

    MaterialCardView cardDinner;
    @BindView(R.id.mPerson_toolbar_list)
    MaterialToolbar mPersonToolbarList;
    @BindView(R.id.tv_trade_item_title)
    TextView tvTradeItemTitle;
    @BindView(R.id.person_recyclerView)
    RecyclerView personRecyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.appbar)
    CoordinatorLayout appbar;
    private String TAG = "DailyFormFragment";
    private HomeViewModel homeViewModel;

    private MPersonListAdapter mPersonListAdapter;


    //butter knife
    private Unbinder unbinder;

    //fragment
    private FragmentManager mFragmentManager;

    List<MPerson> mPersonList = new ArrayList<>();
    View root;
    //db control
    ArrayAdapter<String> adapter;

    public static DailyFormFragment newInstance() {
        return new DailyFormFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_daily_form, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        personRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mPersonList = mPersonService.getAll();

        homeViewModel.getAllMPerson().observe(getViewLifecycleOwner(), new Observer<List<MPerson>>() {
            @Override
            public void onChanged(List<MPerson> mPersons) {
                Log.d("debug_paul", mPersons.toString());
                mPersonList = new ArrayList<>();
                mPersonList = mPersons;
                onRefreshView();
            }
        });

        onRefreshView();
        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_daily_form;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            Snackbar.make(root,  "on Move", BaseTransientBottomBar.LENGTH_LONG).show();
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            int position = viewHolder.getAdapterPosition();
            MPerson mPerson = mPersonList.get(position);
            Snackbar.make(root,  "on delete: " + mPersonList.get(position).getMname(), BaseTransientBottomBar.LENGTH_LONG).show();
            mPersonListAdapter.delModels(position);
            delMPerson(mPerson.getMname(), mPerson.get_id());
        }
    };

    private void onRefreshView() {
        mPersonListAdapter = new MPersonListAdapter(getContext());
        personRecyclerView.setAdapter(mPersonListAdapter);
        initDataList();
        mPersonListAdapter.setOnItemClickListner(onItemClickListner);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(personRecyclerView);
    }

    private MPersonListAdapter.onItemClickListner onItemClickListner = new MPersonListAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, MPerson mPerson) {
            Log.d("debug_paul", "mPerson: " + mPerson);
            Fragment selectedFragment = PersonInfoFragment.newInstance(mPerson.get_id());
            transactionFragment(selectedFragment, Config.PERSON_INFO_FRAGMENT);
        }
    };

    private void initDataList() {
        List<MPersonViewModel> mPersonViewModels = new ArrayList<>();
        for (int i = 0; i < mPersonList.size(); i++) {
            mPersonViewModels.add(new MPersonViewModel(mPersonList.get(i)));
        }
        mPersonListAdapter.addModels(mPersonViewModels);
    }

    /*custom a Fragment back button */
    @Override
    public boolean onBackPressed() {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.backToMain();
        }
        return false;
    }

    public void delMPerson(String name, String id) {
        AndroidNetworking.delete(Config.BASE_URL + Config.API_UPDATE + Config.API_DELETE_PERSON_BY_NAME + "?name=" + name)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            mPersonService.deleteById(id);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    public void transactionFragment(Fragment fragment, String TAG) {
//        mFragmentManager.beginTransaction().replace(R.id.checklist, fragment, Config.LIST_FRAGMENT).addToBackStack(Config.LIST_FRAGMENT).commit();
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DAILY_FORM_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, TAG);
        fragmentTransaction.show(fragment);
//        fragmentTransaction.hide(curFragment);
        fragmentTransaction.addToBackStack(Config.DAILY_FORM_FRAGMENT);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        Fragment selectedFragment = AddMealFragment.newInstance();
        transactionFragment(selectedFragment, Config.ADD_MEAL_FRAGMENT);
    }

}
