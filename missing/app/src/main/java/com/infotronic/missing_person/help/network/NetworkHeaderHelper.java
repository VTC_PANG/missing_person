package com.infotronic.missing_person.help.network;


import com.infotronic.missing_person.config.Config;

import java.util.HashMap;
import java.util.Map;

public class NetworkHeaderHelper {

    public static Map<String, String> getDefaultHeader(){
        Map<String, String> header = new HashMap();
        header.put("Content-Type","application/json");
        header.put("Cache-Control", "no-cache");
        return header;
    }

    public static Map<String, String> getCurrentHeader(){
        Map<String, String> header = new HashMap();
        header.put("Content-Type","application/json");
        header.put("Cache-Control", "no-cache");
        header.put("Authorization", Config.getTokenAccess());

        return header;
    }


}
