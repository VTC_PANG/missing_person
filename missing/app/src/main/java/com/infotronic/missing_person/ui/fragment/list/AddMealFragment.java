package com.infotronic.missing_person.ui.fragment.list;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.help.appbase.FragmentBackHandler;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.adapter.MealListAdapter;
import com.infotronic.missing_person.ui.fragment.HomeViewModel;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.Response;

public class AddMealFragment extends FragmentBase implements FragmentBackHandler {

    CoordinatorLayout appbar;

    MaterialCardView cardSelect;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.et_person_mname)
    EditText etPersonMname;
    @BindView(R.id.et_person_ename)
    EditText etPersonEname;
    @BindView(R.id.et_person_face)
    EditText etPersonFace;
    @BindView(R.id.et_person_wear)
    EditText etPersonWear;
    @BindView(R.id.et_age)
    EditText etAge;
    @BindView(R.id.et_gender)
    EditText etGender;
    @BindView(R.id.et_person_last_location)
    EditText etPersonLastLocation;
    @BindView(R.id.add_item_cancel)
    MaterialButton addItemCancel;
    @BindView(R.id.add_item_confirm)
    MaterialButton addItemConfirm;
    @BindView(R.id.et_person_other_info)
    EditText etPersonOtherInfo;
    private String TAG = "MealListFragment";
    private HomeViewModel homeViewModel;
    private static final String ARG_TYPE = "ARG_TYPE";
    private static final String ARG_COLOR = "ARG_COLOR";
    private String type;
    private String color;

    private MealListAdapter mealListAdapter;

    //butter knife
    private Unbinder unbinder;

    //fragment
    private FragmentManager mFragmentManager;
    View root;
    //db control
    ArrayAdapter<String> adapter;

    public static AddMealFragment newInstance() {
        return new AddMealFragment();
    }

    public static AddMealFragment newInstance(String type) {
        AddMealFragment mealListFragment = new AddMealFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        mealListFragment.setArguments(args);
        return mealListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_add_meal, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_add_meal;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*custom a Fragment back button */
    @Override
    public boolean onBackPressed() {
        onBack();
        return false;

    }

    public void onBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DAILY_FORM_FRAGMENT);
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.ADD_MEAL_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBack();
        }
    };

    public void transactionFragment(Fragment fragment) {
//        mFragmentManager.beginTransaction().replace(R.id.checklist, fragment, Config.LIST_FRAGMENT).addToBackStack(Config.LIST_FRAGMENT).commit();
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.MEAL_LIST_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.ADD_MEAL_FRAGMENT);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.addToBackStack(Config.MEAL_LIST_FRAGMENT);
        fragmentTransaction.commit();
    }

    public void addMPerson() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());
        Map map = new HashMap();
        String mname, emname, age, gender, face, wear, last_location, other, mdate, case_id, status, name2, other2, img, url, revisedDate;
        mname = etPersonMname.getText().toString().trim();
        emname = etPersonEname.getText().toString().trim();
        age = etAge.getText().toString().trim();
        gender = etGender.getText().toString().trim();
        face = etPersonFace.getText().toString().trim();
        wear = etPersonWear.getText().toString().trim();
        last_location = etPersonLastLocation.getText().toString().trim();
        other = etPersonOtherInfo.getText().toString().trim();
        map.put("mname", mname);
        map.put("emname", emname);
        map.put("age", age);
        map.put("gender", gender);
        map.put("face", face);
        map.put("wear", wear);
        map.put("last_location", last_location);
        map.put("other", other);
        map.put("mdate", today);
        map.put("case_id", "");
        map.put("status", "未報案");
        map.put("name2", "");
        map.put("other2", "");
        map.put("img", "");
        map.put("url", "");
        map.put("revisedDate", "");
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.post(Config.BASE_URL + Config.API_SYNC + Config.API_ADD_PERSON)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            Snackbar.make(root,  "successful", 1).show();
                            getMPerson(mname);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    public void getMPerson(String name) {
        AndroidNetworking.get(Config.BASE_URL + Config.API_SYNC + Config.API_GET_PERSON_BY_NAME + "?name=" + name)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            mPersonService.insertAll(response.getResult().getmPerson());
                            onBack();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    @OnClick({R.id.add_item_cancel, R.id.add_item_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_item_cancel:
                onBack();
                break;
            case R.id.add_item_confirm:
                addMPerson();
                break;
        }
    }
}
