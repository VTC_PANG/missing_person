package com.infotronic.missing_person.service.db.impl;

import android.content.Context;
import android.os.AsyncTask;


import com.infotronic.missing_person.db.AppDatabase;
import com.infotronic.missing_person.db.dao.UsersDao;
import com.infotronic.missing_person.db.entity.User;
import com.infotronic.missing_person.service.UsersService;

import java.util.concurrent.ExecutionException;

public class UsersServiceImpl implements UsersService {
    private UsersDao usersDao;

    public UsersServiceImpl(Context context) {
        usersDao = AppDatabase.getInstance(context).usersDao();
    }

    @Override
    public User getById(final String id) {
        try {
            User user = new AsyncTask<Void, Void, User>() {
                @Override
                protected User doInBackground(Void... voids) {
                    return usersDao.getById(id);
                }
            }.execute().get();
            return user;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getByUsername(String uname) {
        try {
            User user = new AsyncTask<Void, Void, User>() {
                @Override
                protected User doInBackground(Void... voids) {
                    return usersDao.getByUsername(uname);
                }
            }.execute().get();
            return user;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertAll(final User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                usersDao.insertAll(user);
                return null;
            }
        }.execute();
    }

    @Override
    public void update(final User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                usersDao.update(user);
                return null;
            }
        }.execute();
    }
}
