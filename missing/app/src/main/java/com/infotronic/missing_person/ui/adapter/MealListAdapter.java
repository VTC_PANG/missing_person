package com.infotronic.missing_person.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.ui.viewModel.MealListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MealListAdapter extends RecyclerView.Adapter {
    Context context;
    List<MealListViewModel> mealListViewModels;

    onItemClickListner onItemClickListner;


    public MealListAdapter(Context context) {
        this.context = context;
        mealListViewModels = new ArrayList<>();
    }

    public void addModels(List<MealListViewModel> testViewModels) {
        this.mealListViewModels = testViewModels;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_meal_list, parent, false);
        return new ItemHolder(row);
    }

    public interface onItemClickListner {
        void onClick(int pos, String description);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MealListViewModel mCurrentItem = mealListViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        itemHolder.tvFoodNamet.setText(mCurrentItem.description);
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.description);
                }
            }
        });
    }




    @Override
    public int getItemCount() {
        return mealListViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.tv_food_namet)
        TextView tvFoodNamet;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
