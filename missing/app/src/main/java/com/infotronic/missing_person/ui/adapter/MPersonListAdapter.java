package com.infotronic.missing_person.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.ui.viewModel.MPersonViewModel;
import com.infotronic.missing_person.ui.viewModel.MealListViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MPersonListAdapter extends RecyclerView.Adapter {
    Context context;
    List<MPersonViewModel> mPersonViewModels;

    onItemClickListner onItemClickListner;
    onItemLongClickListner onItemLongClickListner;


    public MPersonListAdapter(Context context) {
        this.context = context;
        mPersonViewModels = new ArrayList<>();
    }

    public void addModels(List<MPersonViewModel> testViewModels) {
        this.mPersonViewModels = testViewModels;
        notifyDataSetChanged();
    }
    public void delModels(int pos) {
        mPersonViewModels.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_meal_list, parent, false);
        return new ItemHolder(row);
    }

    public interface onItemClickListner {
        void onClick(int pos, MPerson mPerson);//pass your object types.
    }

    public interface onItemLongClickListner {
        void onClick(int pos, MPerson mPerson);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }
    public void setOnItemLongClickListner(onItemLongClickListner onItemLongClickListner) {
        this.onItemLongClickListner = onItemLongClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MPersonViewModel mCurrentItem = mPersonViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        itemHolder.tvFoodNamet.setText(mCurrentItem.mPerson.getMname());
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListner.onClick(position, mCurrentItem.mPerson);
            }
        });
        itemHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onItemLongClickListner.onClick(position, mCurrentItem.mPerson);
                return true;
            }
        });
    }




    @Override
    public int getItemCount() {
        return mPersonViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.tv_food_namet)
        TextView tvFoodNamet;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
