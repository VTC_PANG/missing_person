package com.infotronic.missing_person.service;


import com.infotronic.missing_person.db.entity.User;

public interface UsersService {
    User getById(String id);

    User getByUsername(String uname);

    void insertAll(User user);

    void update(User user);
}
