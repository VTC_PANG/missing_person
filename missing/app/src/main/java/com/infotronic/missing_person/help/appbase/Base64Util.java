package com.infotronic.missing_person.help.appbase;

import android.util.Base64;

import java.util.Random;

public class Base64Util {
    private static final String ALLOWED_CHARACTERS ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String encodeBase64(String str){
        return new String(Base64.encode(str.trim().getBytes(), Base64.DEFAULT));
    }

    private static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    public static String mixRanStr(String str){
        return Base64Util.getRandomString(4) + str + Base64Util.getRandomString(4);
    }

    public static String getEncodeStr(String uname, String pwd){
        //make string to base64 String
        String uNameBase64 = Base64Util.encodeBase64(uname);
        //replace base64 String "=" to ""
        String replaceUname = uNameBase64.replace("=", "").trim();
        //add 4 random char at both sides (e.g. abceXXXXXfghi)
        String ranUname = Base64Util.mixRanStr(replaceUname).trim();

        //make string to base64 String
        String pwdBase64 = Base64Util.encodeBase64(pwd);
        //replace base64 String "=" to ""
        String replacePwd = pwdBase64.replace("=", "").trim();
        //add 4 random char at both sides (e.g. abceXXXXXfghi)
        String ranPwd = Base64Util.mixRanStr(replacePwd).trim();

        //return a new string made by ranUname + "." + ranPwd (e.g. abceXXXXXfghi.BUsdxxVIUB)
        return ranUname + "." + ranPwd;
    }

    //decode
    public static String decodeBase64(String str){
        return new String(Base64.decode(str.trim().getBytes(), Base64.DEFAULT));
    }

    public static String[] splitEncodeStr(String str){
        return str.split("\\.");
    }

    public static String getUname(String str){
        String uname;
        String [] split = splitEncodeStr(str);
        uname = decodeBase64(deReplace(deMixRanStr(split[0])));
        return uname;
    }
    public static String getPwd(String str){
        String pwd;
        String [] split = splitEncodeStr(str);
        pwd = decodeBase64(deReplace(deMixRanStr(split[1])));
        return pwd;
    }

    public static String deMixRanStr(String str){
        return str.substring(4, str.length() - 4);
    }

    public static String deReplace(String str){
        int len = str.length();
           if(len % 4 != 0) {
               switch (len % 4) {
                   case 2:
                       return str + "==";
                   case 3:
                       return str + "=";
               }
           }
        return str;
    }


}
