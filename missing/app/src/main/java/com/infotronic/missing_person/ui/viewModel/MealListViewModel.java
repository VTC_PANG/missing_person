package com.infotronic.missing_person.ui.viewModel;

import androidx.lifecycle.ViewModel;

public class MealListViewModel extends ViewModel {
    public String description;

    public MealListViewModel() {}

    public MealListViewModel(String description) {
        this.description = description;
    }

}