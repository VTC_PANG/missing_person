package com.infotronic.missing_person.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;

import java.util.List;

@Dao
public interface MPersonDao {
    @Query("SELECT * FROM MPerson WHERE _id = :id")
    MPerson getById(String id);

    @Query("SELECT * FROM MPerson WHERE gender = :gender")
    List<MPerson> getByGender(String gender);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MPerson> mPersonList);

    @Insert()
    void insert(MPerson mPerson);

    @Query("DELETE FROM MPerson WHERE _id = :id")
    void deleteById(String id);

    @Update
    void update(MPerson mPerson);

    @Query("SELECT * FROM MPerson")
    List<MPerson> getAll();

    @Query("SELECT * FROM MPerson")
    LiveData<List<MPerson>> getAllMPerson();
}
