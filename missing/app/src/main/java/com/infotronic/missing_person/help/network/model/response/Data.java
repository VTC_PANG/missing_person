package com.infotronic.missing_person.help.network.model.response;


import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.db.entity.User;

import java.io.Serializable;
import java.util.List;

public class Data implements Serializable {
    private String token;

    private User user;
    private List<Foodlibs> foodLibs;
    private List<MPerson> mPerson;


    private Boolean update;


    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public List<Foodlibs> getFoodLibs() {
        return foodLibs;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setFoodLibs(List<Foodlibs> foodlibs) {
        this.foodLibs = foodlibs;
    }

    public List<MPerson> getmPerson() {
        return mPerson;
    }

    public void setmPerson(List<MPerson> mPerson) {
        this.mPerson = mPerson;
    }

    @Override
    public String toString() {
        return "Data{" +
                "token='" + token + '\'' +
                ", user=" + user +
                ", foodLibs=" + foodLibs +
                ", mPersonList=" + mPerson +
                ", update=" + update +
                '}';
    }
}
