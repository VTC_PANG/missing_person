package com.infotronic.missing_person.help.network.model.response;


public class ResponseResult {
    private String status;
    private String code;
    private String currentTime;
    private Data data;
    private String message;
    private String token;

    public ResponseResult(String status, String code, String currentTime, Data data, String message, String token) {
        this.status = status;
        this.code = code;
        this.currentTime = currentTime;
        this.data = data;
        this.message = message;
        this.token = token;
    }

    @Override
    public String toString() {
        return "ResponseResult{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", currentTime='" + currentTime + '\'' +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public Data getResult() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
