package com.infotronic.missing_person.ui.fragment.list;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.help.appbase.FragmentBackHandler;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.adapter.MealListAdapter;
import com.infotronic.missing_person.ui.fragment.HomeViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.Response;

public class PersonInfoFragment extends FragmentBase implements FragmentBackHandler {

    CoordinatorLayout appbar;

    MaterialCardView cardSelect;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.et_person_mname)
    EditText etPersonMname;
    @BindView(R.id.et_person_ename)
    EditText etPersonEname;
    @BindView(R.id.et_person_face)
    EditText etPersonFace;
    @BindView(R.id.et_person_wear)
    EditText etPersonWear;
    @BindView(R.id.et_age)
    EditText etAge;
    @BindView(R.id.et_gender)
    EditText etGender;
    @BindView(R.id.et_person_last_location)
    EditText etPersonLastLocation;
    @BindView(R.id.et_person_other_info)
    EditText etPersonOtherInfo;
    @BindView(R.id.item_update)
    MaterialButton itemUpdate;
    @BindView(R.id.add_item_cancel)
    MaterialButton addItemCancel;

    private String TAG = "MealListFragment";
    private HomeViewModel homeViewModel;
    private static final String ARG_TYPE = "ARG_TYPE";
    private static final String ARG_COLOR = "ARG_COLOR";
    private String type;
    private String color;

    private MealListAdapter mealListAdapter;

    //butter knife
    private Unbinder unbinder;

    //fragment
    private FragmentManager mFragmentManager;
    View root;
    //db control
    ArrayAdapter<String> adapter;
    MPerson current;

    public static PersonInfoFragment newInstance() {
        return new PersonInfoFragment();
    }

    public static PersonInfoFragment newInstance(String type) {
        PersonInfoFragment mealListFragment = new PersonInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        mealListFragment.setArguments(args);
        return mealListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_person_info, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        current = mPersonService.getById(type);
        setup();
        return root;
    }

    public void setup() {
        etPersonMname.setText(current.getMname());
        etPersonEname.setText(current.getEmname());
        etAge.setText(current.getAge());
        etGender.setText(current.getGender());
        etPersonFace.setText(current.getFace());
        etPersonLastLocation.setText(current.getLast_location());
        etPersonWear.setText(current.getWear());
        etPersonOtherInfo.setText(current.getOther());
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_person_info;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*custom a Fragment back button */
    @Override
    public boolean onBackPressed() {
        onBack();
        return false;

    }

    public void onBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DAILY_FORM_FRAGMENT);
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.PERSON_INFO_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBack();
        }
    };

    public void updateMPerson() {
        Map map = new HashMap();
        String mname, emname, age, gender, face, wear, last_location, other;
        mname = etPersonMname.getText().toString().trim();
        emname = etPersonEname.getText().toString().trim();
        age = etAge.getText().toString().trim();
        gender = etGender.getText().toString().trim();
        face = etPersonFace.getText().toString().trim();
        wear = etPersonWear.getText().toString().trim();
        last_location = etPersonLastLocation.getText().toString().trim();
        other = etPersonOtherInfo.getText().toString().trim();
        map.put("mname", mname);
        map.put("emname", emname);
        map.put("age", age);
        map.put("gender", gender);
        map.put("face", face);
        map.put("wear", wear);
        map.put("last_location", last_location);
        map.put("other", other);
        map.put("mdate", current.getMdate());
        map.put("case_id", current.getCase_id());
        map.put("status", current.getStatus());
        map.put("name2", current.getName2());
        map.put("other2", current.getOther2());
        map.put("img", current.getImg());
        map.put("url", current.getUrl());
        map.put("revisedDate", current.getRevisedDate());
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.put(Config.BASE_URL + Config.API_UPDATE + Config.API_UPDATE_PERSON_BY_NAME)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            Snackbar.make(root, "successful", 1).show();
                            getMPerson(mname);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    public void getMPerson(String name) {
        AndroidNetworking.get(Config.BASE_URL + Config.API_SYNC + Config.API_GET_PERSON_BY_NAME + "?name=" + name)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        if ("200".equals(response.getStatus())) {
                            mPersonService.insertAll(response.getResult().getmPerson());
                            onBack();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                    }
                });
    }

    @OnClick({R.id.add_item_cancel, R.id.item_update})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_item_cancel:
                onBack();
                break;
            case R.id.item_update:
                updateMPerson();
                break;
        }
    }
}
