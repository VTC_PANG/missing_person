package com.infotronic.missing_person.ui.fragment;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.service.db.impl.FoodlibsServiceImpl;
import com.infotronic.missing_person.service.db.impl.MPersonServiceImpl;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {

    public String description;
    private LiveData<List<MPerson>> allMPerson;
    private MPersonServiceImpl mPersonService;


    public HomeViewModel(@NonNull Application application) {
        super(application);
        mPersonService = new MPersonServiceImpl(application);
        allMPerson = mPersonService.getAllMPerson();

    }


    public LiveData<List<MPerson>> getAllMPerson() {
        return allMPerson;
    }

}