package com.infotronic.missing_person.service;

import androidx.lifecycle.LiveData;

import com.infotronic.missing_person.db.entity.Foodlibs;

import java.util.List;

public interface FoodlibsService {
    Foodlibs getById(int foodlib_id);

    List<Foodlibs> getByType(String type);

    List<Foodlibs> getBySubType(String subtype);

    LiveData<List<Foodlibs>> getAllFoodblis();

    void insertAll(List<Foodlibs> foodlibs);

    void insert(Foodlibs foodlibs);

    void update(Foodlibs foodlibs);
}
