package com.infotronic.missing_person.ui.fragment.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.LaunchScreenActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import okhttp3.Response;

public class RegisterFragment extends FragmentBase {

    @BindView(R.id.Login_TextView_1)
    TextView LoginTextView1;
    @BindView(R.id.Login_TextView_1_error)
    TextView LoginTextView1Error;
    @BindView(R.id.tf_userName)
    EditText tfUserName;
    @BindView(R.id.Login_TextView_2)
    TextView LoginTextView2;
    @BindView(R.id.Login_TextView_2_error)
    TextView LoginTextView2Error;
    @BindView(R.id.tf_pwd)
    EditText tfPwd;
    @BindView(R.id.Login_TextView_3)
    TextView LoginTextView3;
    @BindView(R.id.Login_TextView_3_error)
    TextView LoginTextView3Error;
    @BindView(R.id.tf_confirm_pwd)
    EditText tfConfirmPwd;
    @BindView(R.id.Login_TextView_4)
    TextView LoginTextView4;
    @BindView(R.id.Login_TextView_4_error)
    TextView LoginTextView4Error;
    @BindView(R.id.tf_email)
    EditText tfEmail;

    @BindView(R.id.register)
    Button register;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    private String mUsername;
    private String mPassword;
    private String gender = "M";
    boolean pass = true;

    private Unbinder unbinder;
    private FragmentManager mFragmentManager;
    private Timer timer = new Timer();
    private final long DELAY = 1000; // milliseconds

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        init();

        getActivity().getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        return root;
    }


    @Override
    public int getFragmentId() {
        return R.layout.fragment_register;
    }

    private void onBack() {
        Fragment nextFragment = LoginFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, nextFragment);
        fragmentTransaction.commit();
    }

    public void init() {
        //set login textView button Onclick Listener
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUsername = tfUserName.getText().toString().trim();
                mPassword = tfPwd.getText().toString().trim();
                String confirmPwd = tfConfirmPwd.getText().toString().trim();
                String email = tfEmail.getText().toString().trim();
                String error = getResources().getString(R.string.emptyColError);
                pass = true;

                if (mUsername.matches("")) {
                    LoginTextView1Error.setVisibility(View.VISIBLE);
                    pass = false;
                } else {
                }

                if (mPassword.matches("")) {
                    LoginTextView2Error.setVisibility(View.VISIBLE);
                    pass = false;
                } else {
                    LoginTextView2Error.setVisibility(View.GONE);
                    checkPwdMatch(mPassword, confirmPwd);
                }
                if (confirmPwd.matches("")) {
                    LoginTextView3Error.setVisibility(View.VISIBLE);
                    pass = false;
                } else {
                    LoginTextView3Error.setVisibility(View.GONE);
                    checkPwdMatch(mPassword, confirmPwd);

                }
                if (email.matches("")) {
                    LoginTextView4Error.setVisibility(View.VISIBLE);
                    pass = false;
                } else {
                    LoginTextView4Error.setVisibility(View.GONE);
                }

                Log.d("debug_paul", "pass: " +pass);


//                register("test", "1234", "M", 18, 170.0, 65.0, "12345678");
                if (pass) {
                    if (getActivity() instanceof LaunchScreenActivity) {
                        LaunchScreenActivity launchScreenActivity = (LaunchScreenActivity) getActivity();
                        launchScreenActivity.register(mUsername, mPassword, email);
                    }
                }
            }
        });
    }

    @OnTextChanged({R.id.tf_confirm_pwd, R.id.tf_pwd})
    protected void afterConfirmPwdTextChanged() {
        String pwd = tfPwd.getText().toString().trim();
        String confirmPwd = tfConfirmPwd.getText().toString().trim();
        checkPwdMatch(pwd, confirmPwd);
    }

    public void checkPwdMatch(String pwd, String confirmPwd){

        if (!pwd.matches("") && !confirmPwd.matches("")) {
            if (!pwd.equals(confirmPwd)) {
                setError();
                pass = false;
            } else {
                LoginTextView3Error.setVisibility(View.GONE);
                LoginTextView2Error.setVisibility(View.GONE);
            }
        }
    }

    public void setError() {
        LoginTextView3Error.setText("The password not match");
        LoginTextView3Error.setVisibility(View.VISIBLE);
        LoginTextView2Error.setText("The password not match");
        LoginTextView2Error.setVisibility(View.VISIBLE);
    }


    public void checkUserName(String uname) {
        Map map = new HashMap();
        map.put("username", uname);
        JSONObject jsonObject = new JSONObject(map);
        AndroidNetworking.post(Config.BASE_URL + Config.API_UPDATE + Config.API_CHECK_USER_NAME)
                .addHeaders(NetworkHeaderHelper.getCurrentHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
                    @Override
                    public void onResponse(Response okHttpResponse, ResponseResult response) {
                        LoginTextView1Error.setTextColor(getResources().getColor(R.color.colorGreen));
                        LoginTextView1Error.setText("("+response.getMessage()+")");
                        LoginTextView1Error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("debug_paul", "anError:" + anError.getErrorBody());
                        Log.d("debug_paul", "anError.getErrorDetail():" + anError.getErrorDetail());
                        Log.d("debug_paul", "anError.getErrorCode():" + anError.getErrorCode());
                        Log.d("debug_paul", "anError.getMessage():" + anError.getMessage());
                        LoginTextView1Error.setTextColor(getResources().getColor(R.color.colorRed));
                        LoginTextView1Error.setText("(Username already exist)");
                        LoginTextView1Error.setVisibility(View.VISIBLE);
                        pass = false;
                    }
                });
    }



}
