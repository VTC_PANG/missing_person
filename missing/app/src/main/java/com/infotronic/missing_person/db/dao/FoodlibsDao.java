package com.infotronic.missing_person.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.missing_person.db.entity.Foodlibs;

import java.util.List;

@Dao
public interface FoodlibsDao {
    @Query("SELECT * FROM Foodlibs WHERE foodlib_id = :foodlib_id")
    Foodlibs getById(int foodlib_id);

    @Query("SELECT * FROM Foodlibs WHERE type = :type")
    List<Foodlibs> getByType(String type);

    @Query("SELECT * FROM Foodlibs WHERE subtype = :subtype")
    List<Foodlibs> getBySubType(String subtype);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Foodlibs> foodlibs);

    @Insert()
    void insert(Foodlibs foodlibs);

    @Update
    void update(Foodlibs foodlibs);

    @Query("SELECT * FROM Foodlibs")
    LiveData<List<Foodlibs>> getAllFoodblis();
}
