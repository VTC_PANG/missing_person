package com.infotronic.missing_person.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MPerson {
    @PrimaryKey
    @NonNull
    private String _id;

    @ColumnInfo(name = "case_id")
    private String case_id;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "mname")
    private String mname;

    @ColumnInfo(name = "emname")
    private String emname;

    @ColumnInfo(name = "name2")
    private String name2;

    @ColumnInfo(name = "face")
    private String face;

    @ColumnInfo(name = "mdate")
    private String mdate;

    @ColumnInfo(name = "reporteddate")
    private String reporteddate;

    @ColumnInfo(name = "wear")
    private String wear;

    @ColumnInfo(name = "other")
    private String other;

    @ColumnInfo(name = "other2")
    private String other2;

    @ColumnInfo(name = "sn")
    private String sn;

    @ColumnInfo(name = "img")
    private String img;

    @ColumnInfo(name = "url")
    private String url;

    @ColumnInfo(name = "revisedDate")
    private String revisedDate;

    @ColumnInfo(name = "age")
    private String age;

    @ColumnInfo(name = "region")
    private String region;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "last_location")
    private String last_location;


    public MPerson(String _id, String case_id, String status, String mname, String emname, String name2, String face, String mdate, String reporteddate, String wear, String other, String other2, String sn, String img, String url, String revisedDate, String age, String region, String gender, String last_location) {
        this._id = _id;
        this.case_id = case_id;
        this.status = status;
        this.mname = mname;
        this.emname = emname;
        this.name2 = name2;
        this.face = face;
        this.mdate = mdate;
        this.reporteddate = reporteddate;
        this.wear = wear;
        this.other = other;
        this.other2 = other2;
        this.sn = sn;
        this.img = img;
        this.url = url;
        this.revisedDate = revisedDate;
        this.age = age;
        this.region = region;
        this.gender = gender;
        this.last_location = last_location;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCase_id() {
        return case_id;
    }

    public void setCase_id(String case_id) {
        this.case_id = case_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getEmname() {
        return emname;
    }

    public void setEmname(String emname) {
        this.emname = emname;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getReporteddate() {
        return reporteddate;
    }

    public void setReporteddate(String reporteddate) {
        this.reporteddate = reporteddate;
    }

    public String getWear() {
        return wear;
    }

    public void setWear(String wear) {
        this.wear = wear;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getOther2() {
        return other2;
    }

    public void setOther2(String other2) {
        this.other2 = other2;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRevisedDate() {
        return revisedDate;
    }

    public void setRevisedDate(String revisedDate) {
        this.revisedDate = revisedDate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLast_location() {
        return last_location;
    }

    public void setLast_location(String last_location) {
        this.last_location = last_location;
    }

    @Override
    public String toString() {
        return
                " mname='" + mname + '\'' +
                "\n emname='" + emname + '\'' +
                "\n age='" + age + '\'' +
                "\n gender='" + gender + '\''+
                "\n name2='" + name2 + '\'' +
                "\n face='" + face + '\'' +
                "\n mdate='" + mdate + '\'' +
                "\n reporteddate='" + reporteddate + '\'' +
                "\n wear='" + wear + '\'' +
                "\n other='" + other + '\'' +
                "\n other2='" + other2 + '\'' +
                "\n revisedDate='" + revisedDate + '\'' +
                "\n status='" + status + '\'' ;

    }
}
