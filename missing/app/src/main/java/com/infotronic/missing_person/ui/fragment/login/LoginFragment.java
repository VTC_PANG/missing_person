package com.infotronic.missing_person.ui.fragment.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.help.appbase.Base64Util;
import com.infotronic.missing_person.help.network.NetworkHeaderHelper;
import com.infotronic.missing_person.help.network.model.response.ResponseResult;
import com.infotronic.missing_person.ui.FragmentBase;
import com.infotronic.missing_person.ui.LaunchScreenActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import okhttp3.Response;

public class LoginFragment extends FragmentBase {
    @BindView(R.id.logo)
    ImageView Logo;
    @BindView(R.id.tf_userName)
    EditText tfUserName;
    @BindView(R.id.tf_pwd)
    EditText tfPwd;
    @BindView(R.id.Login_CheckBox_Remember_Me)
    CheckBox LoginCheckBoxRememberMe;
    @BindView(R.id.Login_Button_Login)
    TextView LoginButtonLogin;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.Login_TextView_1)
    TextView LoginTextView1;
    @BindView(R.id.Login_TextView_2)
    TextView LoginTextView2;
    @BindView(R.id.Login_TextView_1_error)
    TextView LoginTextView1Error;
    @BindView(R.id.Login_TextView_2_error)
    TextView LoginTextView2Error;

    @BindView(R.id.register)
    Button register;
    private String mUsername;
    private String mPassword;

    private FragmentManager mFragmentManager;

    private Unbinder unbinder;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        init();
        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_login;
    }


    @OnTextChanged(value = {R.id.tf_userName, R.id.tf_pwd}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onChange() {
        mUsername = tfUserName.getText().toString();
        mPassword = tfPwd.getText().toString();
    }

    public void init() {
        //set login textView button Onclick Listener
        setVersion();
        LoginButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof LaunchScreenActivity) {
                    LaunchScreenActivity launchScreenActivity = (LaunchScreenActivity) getActivity();
                    mUsername = tfUserName.getText().toString().trim();
                    mPassword = tfPwd.getText().toString().trim();

                    if (mUsername.matches("")) {
                        LoginTextView1Error.setVisibility(View.VISIBLE);
                    } else {
                        LoginTextView1Error.setVisibility(View.GONE);
                    }

                    if (mPassword.matches("")) {
                        LoginTextView2Error.setVisibility(View.VISIBLE);
                    } else {
                        LoginTextView2Error.setVisibility(View.GONE);
                    }

                    if (!mUsername.matches("") && !mPassword.matches("")) {
                        boolean ischeck = false;
                        if (LoginCheckBoxRememberMe.isChecked()) {
                            ischeck = true;
                        }

                        launchScreenActivity.login(mUsername, mPassword, ischeck);

                    }
                }
            }
        });
    }

    public void setVersion() {
        tvVersion.setText("Version 1.0.0");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.register)
    public void onViewClicked() {
        Fragment nextFragment = RegisterFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, nextFragment);
        fragmentTransaction.commit();
    }
}
