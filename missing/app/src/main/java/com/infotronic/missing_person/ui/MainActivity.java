package com.infotronic.missing_person.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.infotronic.missing_person.R;
import com.infotronic.missing_person.config.Config;
import com.infotronic.missing_person.help.appbase.BackHandlerHelper;
import com.infotronic.missing_person.ui.fragment.home.HomeFragment;
import com.infotronic.missing_person.ui.fragment.list.DailyFormFragment;
import com.infotronic.missing_person.ui.fragment.setting.SettingFragment;
import com.infotronic.missing_person.util.AcitvityUtil;


public class MainActivity extends ActivityBase {
    private FragmentManager mFragmentManager;
    BottomNavigationView bottomNavigationView;
    SharedPreferences settings;

    @Override
    public Context getActivityContext() {
        return MainActivity.this;
    }

    @Override
    public int getContentViewRid() {
        return R.layout.activity_main;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getSupportFragmentManager();
        settings = getSharedPreferences(Config.PREF_KEY, MODE_PRIVATE);
        String userId = settings.getString(Config.PREF_USER_ID_KEY, "");
        Config.setUid(userId);

        loadInitialFragment();

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.home:
                Log.d("debug_paul", "home");
                selectedFragment = new HomeFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.HOME_FRAGMENT).commitAllowingStateLoss();
                break;

            case R.id.list:
                Log.d("debug_paul", "list");
                selectedFragment = new DailyFormFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.DAILY_FORM_FRAGMENT).commitAllowingStateLoss();
                break;
            case R.id.setting:
                Log.d("debug_paul", "setting");
                selectedFragment = new SettingFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.SETTING_FRAGMENT).commitAllowingStateLoss();
                break;

        }
        return true;
    };

    public void logout() {
        Config.flushData();
        AcitvityUtil.delectLoginSP(getApplicationContext());
        Intent intentLogout = new Intent(getApplicationContext(), LaunchScreenActivity.class);
        startActivity(intentLogout);
        finish();
    }

    private void loadInitialFragment() {
        Fragment initialFragment = HomeFragment.newInstance();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, initialFragment, Config.HOME_FRAGMENT);
        fragmentTransaction.commit();
    }

    public void backToMain() {
        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        Fragment initialFragment = HomeFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, initialFragment, Config.HOME_FRAGMENT);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (!BackHandlerHelper.handleBackPress(this)) {
            if (!mFragmentManager.popBackStackImmediate()) {
                Fragment curFragment = mFragmentManager.findFragmentByTag(Config.HOME_FRAGMENT);
                if (curFragment == null) {
                    loadInitialFragment();
                    bottomNavigationView.getMenu().getItem(0).setChecked(true);
                }
            }
        }
    }
}
