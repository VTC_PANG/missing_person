package com.infotronic.missing_person.help.appbase;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

public abstract class CustomParsedRequestListener<T> implements ParsedRequestListener<T> {

    private String TAG;
    private CallbackDialog callbackDialog;

    public CustomParsedRequestListener(String TAG, CallbackDialog dismissDialogCallback) {
        this.TAG = TAG;
        this.callbackDialog = dismissDialogCallback;
    }

    protected void onResponseForImp(T response){

    }
    public void onErrorForImp(ANError anError){ }

    @Override
    public void onResponse(T response) {
//        ObjectUtil.printObject(TAG, response);
        onResponseForImp(response);
    }

    @Override
    public void onError(ANError anError) {
        ObjectUtil.printObject(TAG, anError);
        callbackDialog.dismissDialog();
        callbackDialog.errorDialog(anError);
        onErrorForImp(anError);
    }

    public interface CallbackDialog {
        void dismissDialog();
        void errorDialog(ANError anError);
    }

}
