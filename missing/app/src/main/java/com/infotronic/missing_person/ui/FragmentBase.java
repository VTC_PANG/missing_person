package com.infotronic.missing_person.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;


import com.infotronic.missing_person.service.db.impl.FoodlibsServiceImpl;
import com.infotronic.missing_person.service.db.impl.MPersonServiceImpl;
import com.infotronic.missing_person.service.db.impl.UsersServiceImpl;

import org.jetbrains.annotations.NotNull;

public abstract class FragmentBase extends Fragment {
    View view;
    private Dialog dialog;

    //===============db caller=======================================
    public static UsersServiceImpl userService = null;
    public static FoodlibsServiceImpl foodlibsService = null;
    public static MPersonServiceImpl mPersonService = null;

    //===============================================================
    //fragment set up
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanseState) {
        initDbService(getContext());
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.dismissDialog();
        }
        view = fragmentView(inflater, container, savedInstanseState);
        return view;
    }

    public void initDbService(Context context){
        if (userService == null)
            userService = new UsersServiceImpl(context);
        if (foodlibsService == null)
            foodlibsService = new FoodlibsServiceImpl(context);
        if (mPersonService == null)
            mPersonService = new MPersonServiceImpl(context);
    }

    public void resetDbService(){
        userService = null;
        foodlibsService = null;
        mPersonService = null;
        initDbService(getContext());
    }

    public abstract View fragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public abstract int getFragmentId();


}
