package com.infotronic.missing_person.ui.viewModel;

import androidx.lifecycle.ViewModel;

public class TestViewModel extends ViewModel {
    public String description;

    public TestViewModel() {}

    public TestViewModel(String description) {
        this.description = description;
    }

}