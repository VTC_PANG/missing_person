package com.infotronic.missing_person.service.db.impl;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.infotronic.missing_person.db.AppDatabase;
import com.infotronic.missing_person.db.dao.MPersonDao;
import com.infotronic.missing_person.db.entity.Foodlibs;
import com.infotronic.missing_person.db.entity.MPerson;
import com.infotronic.missing_person.service.MPersonService;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MPersonServiceImpl implements MPersonService {
    private MPersonDao mPersonDao;
    private LiveData<List<MPerson>> allMPerson;

    public MPersonServiceImpl(Context context){
        mPersonDao = AppDatabase.getInstance(context).mPersonDao();
        allMPerson = mPersonDao.getAllMPerson();
    }

    @Override
    public MPerson getById(final String id) {
        try {
            MPerson projects = new AsyncTask<Void, Void, MPerson>() {
                @Override
                protected MPerson doInBackground(Void... voids) {
                    return mPersonDao.getById(id);
                }
            }.execute().get();
            return projects;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<MPerson> getByGender(String gender) {
        try {
            List<MPerson> mPersonList = new AsyncTask<Void, Void, List<MPerson>>() {
                @Override
                protected List<MPerson> doInBackground(Void... voids) {
                    return mPersonDao.getByGender(gender);
                }
            }.execute().get();
            return mPersonList;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public LiveData<List<MPerson>> getAllMPerson() {
        return allMPerson;
    }

    @Override
    public void insertAll(final List<MPerson> mPersonList) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                mPersonDao.insertAll(mPersonList);
                return null;
            }
        }.execute();
    }

    @Override
    public void insert(MPerson mPerson) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                mPersonDao.insert(mPerson);
                return null;
            }
        }.execute();
    }

    @Override
    public void update(MPerson mPerson) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                mPersonDao.update(mPerson);
                return null;
            }
        }.execute();
    }

    @Override
    public void deleteById(String id) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    mPersonDao.deleteById(id);
                    return null;
                }
            }.execute();
    }

    @Override
    public List<MPerson> getAll() {
        try {
            List<MPerson> projects = new AsyncTask<Void, Void, List<MPerson>>() {
                @Override
                protected List<MPerson> doInBackground(Void... voids) {
                    return mPersonDao.getAll();
                }
            }.execute().get();
            return projects;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
