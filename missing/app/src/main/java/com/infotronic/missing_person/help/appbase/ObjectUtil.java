package com.infotronic.missing_person.help.appbase;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.Map;

public class ObjectUtil {

    private ObjectUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static Map printObject(String tag, Object object){
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.convertValue(object, Map.class);
        Log.d(tag, "map:" + map);
        return map;
    }

    public static JSONObject objectToJsonObject(String tag, Object object){
        ObjectMapper objectMapper = new ObjectMapper();
        Map map = objectMapper.convertValue(object, Map.class);
        Log.d(tag, "map:" + map);
        return new JSONObject(map);
    }
}
